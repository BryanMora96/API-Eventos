package main

import "fmt"

func pagoCaja(motopago []MotoPago) (error, string) {

	var items string
	var nuSumCant int64
	var separador string = "|"

	bd, err := getDB()
	if err != nil {
		return err, items
	}

	for i := len(motopago) - 1; i >= 0; i-- {
		nuSumCant = nuSumCant + (motopago[i].Valor * motopago[i].Cantidad)
	}

	if nuSumCant < TotaValo() {

		//var sbValoCambio float64 = (float64(nuSumCant) - float64(nuSumCant)*0.10)
		var sbValoCambio float64 = float64(nuSumCant) * 0.10
		fmt.Println("El valor de sbValoCambio es: ", sbValoCambio)

		_, err = bd.Exec("INSERT INTO REGIPAGO (IDPAGO, VALOENTR, VALOSALD) VALUES (?, ?, ?)", motopago[0].IdMopa, (nuSumCant - int64(sbValoCambio)), sbValoCambio)

		for 0 < sbValoCambio {
			if sbValoCambio >= float64(valoBil1) {
				fmt.Println("Un Billete de 100000")
				items = items + "Un Billete de 100000" + separador
				sbValoCambio = sbValoCambio - float64(valoBil1)
				actuValor(1, valoBil1, "S")
			} else if sbValoCambio >= float64(valoBil2) {
				fmt.Println("Un Billete de 50000")
				items = items + "Un Billete de 50000" + separador
				sbValoCambio = sbValoCambio - float64(valoBil2)
				actuValor(1, valoBil2, "S")
			} else if sbValoCambio >= float64(valoBil3) {
				fmt.Println("Un Billete de 20000")
				items = items + "Un Billete de 20000" + separador
				sbValoCambio = sbValoCambio - float64(valoBil3)
				actuValor(1, valoBil3, "S")
			} else if sbValoCambio >= float64(valoBil4) {
				fmt.Println("Un Billete de 10000")
				items = items + "Un Billete de 10000" + separador
				sbValoCambio = sbValoCambio - float64(valoBil4)
				actuValor(1, valoBil4, "S")
			} else if sbValoCambio >= float64(valoBil5) {
				fmt.Println("Un Billete de 5000")
				items = items + "Un Billete de 5000" + separador
				sbValoCambio = sbValoCambio - float64(valoBil5)
				actuValor(1, valoBil5, "S")
			} else if sbValoCambio >= float64(valoMon1) {
				fmt.Println("Una Moneda de 1000")
				items = items + "Una Moneda de 1000" + separador
				sbValoCambio = sbValoCambio - float64(valoMon1)
				actuValor(1, valoMon1, "S")
			} else if sbValoCambio >= float64(valoMon2) {
				fmt.Println("Una Moneda de 500")
				items = items + "Una Moneda de 500" + separador
				sbValoCambio = sbValoCambio - float64(valoMon2)
				actuValor(1, valoMon2, "S")
			} else if sbValoCambio >= float64(valoMon3) {
				fmt.Println("Una Moneda de 200")
				items = items + "Una Moneda de 200" + separador
				sbValoCambio = sbValoCambio - float64(valoMon3)
				actuValor(1, valoMon3, "S")
			} else if sbValoCambio >= float64(valoMon4) {
				fmt.Println("Una Moneda de 100")
				items = items + "Una Moneda de 100" + separador
				sbValoCambio = sbValoCambio - float64(valoMon4)
				actuValor(1, valoMon4, "S")
			} else if sbValoCambio >= float64(valoMon5) || sbValoCambio < float64(valoMon5) {
				fmt.Println("Una Moneda de 50")
				items = items + "Una Moneda de 50" + separador
				sbValoCambio = sbValoCambio - float64(valoMon5)
				actuValor(1, valoMon5, "S")
			}
		}
		for i := len(motopago) - 1; i >= 0; i-- {
			_, err = bd.Exec("INSERT INTO MOTOPAGO (IDMOPA, DIVISA, VALOR, CANTIDAD) VALUES (?, ?, ?, ?)", motopago[i].IdMopa, motopago[i].Divisa, motopago[i].Valor, motopago[i].Cantidad)
			actuValor(float64(motopago[i].Cantidad), motopago[i].Valor, "N")
		}
	}

	return err, items
}

func createVideoGame(divisa Divisa) error {
	bd, err := getDB()
	if err != nil {
		return err
	}

	if ValidaValor(divisa.Valor) == true {
		_, err = bd.Exec("INSERT INTO DIVISAS (DIVISA, VALOR, CANTIDAD) VALUES (?, ?, ?)", divisa.Divisa, divisa.Valor, divisa.Cantidad)
	}

	return err
}

func deleteCaja() error {

	var idDelete string
	idDelete = "COP"

	bd, err := getDB()
	if err != nil {
		return err
	}
	_, err = bd.Exec("DELETE FROM DIVISAS WHERE DIVISA = ?", idDelete)
	return err
}

func getDivisas() ([]Divisa, error) {

	divisas := []Divisa{}
	bd, err := getDB()
	if err != nil {
		return divisas, err
	}

	rows, err := bd.Query("SELECT DIVISA, VALOR, CANTIDAD FROM DIVISAS")
	if err != nil {
		return divisas, err
	}

	for rows.Next() {
		var divisa Divisa
		err = rows.Scan(&divisa.Divisa, &divisa.Valor, &divisa.Cantidad)
		if err != nil {
			return divisas, err
		}
		divisas = append(divisas, divisa)
	}
	return divisas, nil
}

func getMovicaja() ([]Pago, error) {

	pagos := []Pago{}
	bd, err := getDB()
	if err != nil {
		return pagos, err
	}

	rows, err := bd.Query("SELECT IDPAGO, FECHA, VALOENTR, VALOSALD FROM REGIPAGO")
	if err != nil {
		return pagos, err
	}

	for rows.Next() {
		var pago Pago
		err = rows.Scan(&pago.IdPago, &pago.Fecha, &pago.ValorEntrada, &pago.ValorSalida)
		if err != nil {
			return pagos, err
		}
		pagos = append(pagos, pago)
	}
	return pagos, nil
}
