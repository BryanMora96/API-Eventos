package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/bitly/go-simplejson"
	"github.com/gorilla/mux"
)

func setupRoutesForDivisas(router *mux.Router) {
	enableCORS(router)

	router.HandleFunc("/obtedivisa", func(w http.ResponseWriter, r *http.Request) {
		obtDivisa, err := getDivisas()
		if err == nil {
			respondWithSuccess(obtDivisa, " ", w)
		} else {
			respondWithError(err, w)
		}
	}).Methods(http.MethodGet)

	router.HandleFunc("/obteMovicaja", func(w http.ResponseWriter, r *http.Request) {
		obtMovicaja, err := getMovicaja()
		if err == nil {
			respondWithSuccess(obtMovicaja, " ", w)
		} else {
			respondWithError(err, w)
		}
	}).Methods(http.MethodGet)

	router.HandleFunc("/addDivisa", func(w http.ResponseWriter, r *http.Request) {
		var divisa Divisa
		err := json.NewDecoder(r.Body).Decode(&divisa)

		if err != nil {
			respondWithError(err, w)
		} else {
			err := createVideoGame(divisa)
			if err != nil {
				respondWithError(err, w)
			} else {
				if ValidaValor(divisa.Valor) == true {
					respondWithSuccess(true, " ", w)
				} else {
					respondWithSuccess("El valor ingresado no es valido", " ", w)
				}

			}
		}
	}).Methods(http.MethodPost)

	router.HandleFunc("/addPago", func(w http.ResponseWriter, r *http.Request) {
		var nuSumCant int64
		var motopago []MotoPago
		err := json.NewDecoder(r.Body).Decode(&motopago)

		for i := len(motopago) - 1; i >= 0; i-- {
			nuSumCant = nuSumCant + (motopago[i].Valor * motopago[i].Cantidad)
		}

		if err != nil {
			respondWithError(err, w)
		} else {
			err, items := pagoCaja(motopago)
			if err != nil {
				respondWithError(err, w)
			} else {
				if nuSumCant < TotaValo() {
					respondWithSuccess(true, items, w)
				} else {
					respondWithSuccess("No se tiene el suficiente dinero para dar cambio", " ", w)
				}

			}
		}

	}).Methods(http.MethodPost)

	router.HandleFunc("/deleCaja", func(w http.ResponseWriter, r *http.Request) {
		err := deleteCaja()
		if err != nil {
			respondWithError(err, w)
		} else {
			respondWithSuccess("El saldo en la caja es cero", " ", w)
		}
	}).Methods(http.MethodDelete)
}

func enableCORS(router *mux.Router) {
	router.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", AllowedCORSDomain)
	}).Methods(http.MethodOptions)

	router.Use(middlewareCors)
}

func middlewareCors(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, req *http.Request) {
			w.Header().Set("Access-Control-Allow-Origin", AllowedCORSDomain)
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
			w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
			next.ServeHTTP(w, req)
		})
}

func respondWithError(err error, w http.ResponseWriter) {
	w.WriteHeader(http.StatusInternalServerError)
	json.NewEncoder(w).Encode(err.Error())
}

func respondWithSuccess(data interface{}, items string, w http.ResponseWriter) {
	w.WriteHeader(http.StatusOK)

	json := simplejson.New()
	if items != " " {
		json.Set("Cambio", items)
	}
	json.Set("Status", data)

	payload, err := json.MarshalJSON()
	if err != nil {
		log.Println(err)
	}

	w.Header().Set("Content-Type", "applicaction/json")
	w.Write(payload)
}
