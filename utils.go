package main

func ValidaValor(valor int64) bool {
	var sbRetorna bool
	sbRetorna = false
	if valor == valoBil1 ||
		valor == valoBil2 ||
		valor == valoBil3 ||
		valor == valoBil4 ||
		valor == valoBil5 ||
		valor == valoMon1 ||
		valor == valoMon2 ||
		valor == valoMon3 ||
		valor == valoMon4 ||
		valor == valoMon5 {
		sbRetorna = true
	}
	return sbRetorna
}

func TotaValo() int64 {
	var totaSald int64
	bd, err := getDB()
	if err != nil {
		return totaSald
	}

	totalSaldo, err := bd.Query("SELECT sum(valor*cantidad) totaSald FROM DIVISAS")

	for totalSaldo.Next() {
		totalSaldo.Scan(&totaSald)
	}

	return totaSald
}

func actuValor(sbValoCambio float64, sbValor int64, sbValida string) error {
	var cantidad int64
	var calcCant int64
	bd, err := getDB()
	if err != nil {
		return err
	}

	totalSaldo, err := bd.Query("SELECT cantidad FROM DIVISAS WHERE DIVISA = ? AND VALOR = ? ", "COP", sbValor)

	for totalSaldo.Next() {
		totalSaldo.Scan(&cantidad)
		if sbValida == "S" {
			calcCant = cantidad - int64(sbValoCambio)
		} else {
			calcCant = cantidad + int64(sbValoCambio)
		}
		_, err = bd.Exec("UPDATE DIVISAS SET CANTIDAD = ? WHERE DIVISA = ? AND VALOR = ?", calcCant, "COP", sbValor)
	}
	return err

}
