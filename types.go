package main

type Divisa struct {
	Divisa   string `json:"divisa"`
	Valor    int64  `json:"valor"`
	Cantidad int64  `json:"cantidad"`
}

type Pago struct {
	IdPago       string `json:"idpago"`
	Fecha        string `json:"fecha"`
	ValorEntrada int64  `json:"valoentr"`
	ValorSalida  int64  `json:"valosald"`
}

type MotoPago struct {
	IdMopa   string `json:"idmopa"`
	Divisa   string `json:"divisa"`
	Valor    int64  `json:"valor"`
	Cantidad int64  `json:"cantidad"`
}
