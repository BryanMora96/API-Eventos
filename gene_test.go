package main_test

import (
	main "apieventos"
	"testing"
)

func TestValidaValorError(t *testing.T) {
	var boValida bool
	boValida = main.ValidaValor(1000000)

	if boValida {
		t.Fatal("El valor es true")
	}
}

func TestValidaValorSucces(t *testing.T) {
	var boValida bool
	boValida = main.ValidaValor(200)

	if !boValida {
		t.Fatal("El valor es true")
	}
}

func TestCalculoValoTota(t *testing.T) {
	var nuTotaValo int64
	nuTotaValo = main.TotaValo()

	if nuTotaValo == 0 {
		t.Fatal("El valor es cero")
	} else {
		t.Fatal("El valor total del calculo es: ", nuTotaValo)
	}
}
